# Initializes various variables used by the script
SetVars () {
	# Find top level working directory
	SOURCE="${1}";
	while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
		TOPBUILDDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )";
		SOURCE="$(readlink "$SOURCE")";
		[[ $SOURCE != /* ]] && SOURCE="$TOPBUILDDIR/$SOURCE"; # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done;
	SCRIPTSDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )";
	TOPBUILDDIR="$(dirname "${SCRIPTSDIR}")";

	# Set relevant global variables
	MANIFESTDIR=${SCRIPTSDIR}/manifests;
	LINEAGEVER=18.1
	LINEAGEBRANCH="lineage-${LINEAGEVER}";
	LINEAGEDIR=${TOPBUILDDIR}/lineage-${LINEAGEVER};
	TWRPVER=11
	TWRPBRANCH="twrp-${TWRPVER}";
	TWRPDIR=${TOPBUILDDIR}/twrp-${TWRPVER};
	PATCHDIR=${SCRIPTSDIR}/patches;
	ORIGPATH="${PATH}"
	BUILD_TYPE="UNOFFICIAL"
	export JAVA_TOOL_OPTIONS="-Xmx2g -XX:ParallelGCThreads=2"

	if [ ! -d ${LINEAGEDIR}/venv ]; then
		virtualenv -p python3 ${LINEAGEDIR}/venv;
	fi;
	source ${LINEAGEDIR}/venv/bin/activate;

	# Set list of devices if not already set
	if [ ! -f ${SCRIPTSDIR}/devices.txt ]; then
		declare -a BUILDDEVICES=("roth" "shieldtablet" "foster" "jetson");
		for dev in "${BUILDDEVICES[@]}"
		do
			echo ${dev} >> ${SCRIPTSDIR}/devices.txt;
		done;
	fi;
}

# Start up error checking
CommandCheck () {
	CMD="$1";
	MORE="$2";
	command -v ${CMD} >/dev/null 2>&1 || { echo >&2 "${CMD} is required, but not installed."; [ ! -z "${MORE}" ] && echo >&2 "${MORE}"; echo >&2 "Aborting."; return 1; }

	return 0;
}

# Start up error checking
ValidityCheck () {
	# Check parameter validity
	if [ ! -z "$2" ] && [ ! "$2" == "lineage" ] && [ ! "$2" == "twrp" ] && [ ! "$2" == "all" ]; then
		echo "Usage: $1 [lineage or twrp]";
		return 1;
	fi;

	# Check for needed programs
	for COMMAND_CHECK in 'repo' 'gperf' 'bison' 'virtualenv'; do
		CommandCheck ${COMMAND_CHECK}
	done
	CommandCheck 'git-lfs' 'Please consult https://github.com/git-lfs/git-lfs/wiki/Installation .'

	return 0;
}

# Apply or revert a single patch
ApplyPatch () {
	REVERT=
	if [ "$1" == "revert" ]; then 
		REVERT="-R";
		shift;
	fi;

	if [ -d ${2} ] && [ -f ${1} ]; then
		cd ${2};
		git apply ${REVERT} ${1};
		return $?;
	else
		return 1;
	fi;
}

# Revert a set of patches listed in a given file
RevertPatchesFromFile () {
	if [ -f ${PATCHDIR}/${1} ]; then
		ERRORPATCH=${3};
		tempvar=$(tac ${PATCHDIR}/${1});
		while read -r patch pdir; do
			# If reverting due to an error, skip everything up to the cause of the error
			if [ -n "${ERRORPATCH}" ]; then
				if [ "${ERRORPATCH}" == "${patch}" ]; then
					ERRORPATCH=;
				fi;
				continue;
			fi;
			# If the line is for a pick topic, don't revert
			if [ -z "${pdir}" ] || [[ "${pdir}" =~ ^(ssh|http|https)(\:\/\/) ]]; then
				continue;
			fi;
			logadd "Reverting ${patch}";
			ApplyPatch revert ${PATCHDIR}/${patch}.patch ${2}/${pdir};
		done <<< "${tempvar}";
	fi;

	return 0;
}

# Apply a set of patches listed in a given file. If one errors, roll back previous patches.
ApplyPatchesFromFile () {
	if [ -f ${PATCHDIR}/${1} ]; then
		ERRORPATCH=;
		while read -r -u 7 patch pdir; do
			if [ -z "${pdir}" ] || [[ "${pdir}" =~ ^(ssh|http|https)(\:\/\/) ]] || [ "${pdir}" == "-f" ] || [ "${pdir:0:2}" == "-P" ]; then
				if [ "${OFFLINE}" == "true" ]; then
					logadd "Skipping repopick ${patch} because an offline build was requested";
					continue;
				fi;
				GERRITARGS=
				if [ "${pdir}" == "-f" ]; then
					# Second param is force pick
					GERRITARGS="-f";
				elif [ "${pdir:0:2}" == "-P" ]; then
					# Second param is a project path
					GERRITARGS="${pdir}";
				elif [ -n "${pdir}" ]; then
					# Second param is a gerrit url
					GERRITARGS="-g ${pdir}";
				fi;
				cd ${2};
				if [[ ${patch} =~ ^([0-9]+)(\-[0-9]+)?(\/[0-9]+)?$ ]]; then
					logadd "Picking ${patch}";
					repopick ${GERRITARGS} ${patch};
				else
					logadd "Picking topic ${patch}";
					repopick ${GERRITARGS} -t ${patch};
				fi;
				if [ $? -ne 0 ]; then
					ERRORPATCH=${patch};
					break;
				fi;
			else
				logadd "Applying ${patch}";
				if ! ApplyPatch ${PATCHDIR}/${patch}.patch ${2}/${pdir}; then
					ERRORPATCH=${patch};
					break;
				fi;
			fi;
		done 7< ${PATCHDIR}/${1};

		if [ -n "${ERRORPATCH}" ]; then
			logadd "Error applying ${patch}, reverting all grouped patches";
			RevertPatchesFromFile ${1} ${2} ${ERRORPATCH};
			cd ${BASEDIR};
			repo forall -c 'if [[ ! -z $(git status -s) ]]; then git cherry-pick --abort; fi';
			return 1;
		fi;
	fi;

	return 0;
}

# Build the system
build_android () {
	# Name the parameters
	BUILDSYSTEM=${1};
	BASEDIR=${2};
	BUILDTYPE=${3};

	# Set what gets built based on system type
	if [ "${BUILDSYSTEM}" == "twrp" ]; then
		export ALLOW_MISSING_DEPENDENCIES=true;
		export WITH_TWRP=true;
		MAKETARGETS="recoveryimage";
	else
		MAKETARGETS="target-files-package dist otatools";
	fi;
	BUILDPREFIX=${BUILDSYSTEM};

	logadd "Starting builds for ${BUILDSYSTEM}";

	# Save environment so it can be reset later
	saveenv;

	# Set up ccache
	export USE_CCACHE=1;
	export CCACHE_EXEC=$(which ccache);
	if [ -z ${CACHE_DIR_COMMON_BASE} ]; then
		export CCACHE_DIR=${BASEDIR}/ccache;
	else
		export CCACHE_DIR=${CACHE_DIR_COMMON_BASE}/$(basename ${BASEDIR});
	fi;
	if [ ! -d "${CCACHE_DIR}" ]; then
		mkdir "${CCACHE_DIR}";
		ccache -M 50G;
	fi;

	# Set up dist out
	if [ -z ${OUT_DIR_COMMON_BASE} ]; then
		export ANDROID_DIST_OUT=${BASEDIR}/out/dist;
	else
		export ANDROID_DIST_OUT=${OUT_DIR_COMMON_BASE}/${LINEAGEBRANCH}/dist;
	fi;

	# Initialize the build system and clean previous builds
	cd ${BASEDIR};
	source build/envsetup.sh;
	m clean;

	# Apply custom patches
	if ! ApplyPatchesFromFile patches_${BUILDSYSTEM}.txt ${BASEDIR}; then
		echo "Couldn't apply patches for ${BUILDSYSTEM}, skipping builds";
		return 1;
	fi;

	# Loop through devices to build
	while read -r dev; do
		logadd "Building ${dev}";
		# Check if device is valid
		if [ "$(ls -d ${BASEDIR}/device/*/${dev} 2>/dev/null |wc -l)" == "0" ]; then
			echo "Couldn't find device tree for ${dev}, skipping";
			continue;
		fi;

		# Apply special handling patches, if needed
		if ! ApplyPatchesFromFile patches_${BUILDSYSTEM}_${dev}.txt ${BASEDIR}; then
			echo "Failed to apply some patches for ${dev}, skipping";
			continue;
		fi;

		# Build
		cd ${BASEDIR};
		lunch ${BUILDPREFIX}_${dev}-${BUILDTYPE};
		m -j$(expr $(nproc) + 1) ${MAKETARGETS};

		if [ $? -ne 0 ]; then
			RevertPatchesFromFile patches_${BUILDSYSTEM}_${dev}.txt ${BASEDIR};
			continue;
		fi;

		BDATE=$(TZ=UTC date -d @$(grep ro\.build\.date\.utc ${ANDROID_PRODUCT_OUT}/recovery/root/prop.default |awk -F= '{ print $2 }') +%Y%m%d)
		IS_DYNAMIC=$(grep ro\.boot\.dynamic_partitions ${ANDROID_PRODUCT_OUT}/recovery/root/prop.default |awk -F= '{ print $2 }')
		UPLOAD_DIR=${TOPBUILDDIR}/uploads/${BDATE}/${dev};
		mkdir -p ${UPLOAD_DIR};
		if [ "${BUILDSYSTEM}" == "lineage" ] && [[ "${MAKETARGETS}" == *"target-files-package"* ]]; then
			if ls ${ANDROID_PRODUCT_OUT}/obj/PACKAGING/target_files_intermediates/${BUILDSYSTEM}_${dev}-target_files-*/IMAGES/vbmeta.img 1> /dev/null 2>&1; then
				if [ "${IS_DYNAMIC}" == "true" ]; then
					DYNAMIC_SWITCHES=(--avb_vbmeta_system_key ${HOME}/.android-certs/avb.pem
						--avb_vbmeta_system_algorithm SHA256_RSA4096)
				else
					DYNAMIC_SWITCHES=(--avb_system_key ${HOME}/.android-certs/avb.pem
					--avb_system_algorithm SHA256_RSA4096
					--avb_vendor_key ${HOME}/.android-certs/avb.pem
					--avb_vendor_algorithm SHA256_RSA4096
					--avb_extra_custom_image_key odm=${HOME}/.android-certs/avb.pem
					--avb_extra_custom_image_algorithm odm=SHA256_RSA4096)
				fi;
				VERITY_SWITCHES=(--avb_vbmeta_key ${HOME}/.android-certs/avb.pem
					--avb_vbmeta_algorithm SHA256_RSA4096
					--avb_boot_key ${HOME}/.android-certs/avb.pem
					--avb_boot_algorithm SHA256_RSA4096
					--avb_extra_custom_image_key recovery=${HOME}/.android-certs/avb.pem
					--avb_extra_custom_image_algorithm recovery=SHA256_RSA4096
					"${DYNAMIC_SWITCHES[@]}")
			fi;
			${ANDROID_HOST_OUT}/bin/sign_target_files_apks -o -d ~/.android-certs "${VERITY_SWITCHES[@]}" ${ANDROID_DIST_OUT}/lineage_${dev}-target_files-*.zip ${ANDROID_PRODUCT_OUT}/lineage_${dev}-target_files-signed.zip
			${ANDROID_HOST_OUT}/bin/ota_from_target_files -k ~/.android-certs/releasekey --block --backup=true --skip_compatibility_check ${ANDROID_PRODUCT_OUT}/lineage_${dev}-target_files-signed.zip ${UPLOAD_DIR}/lineage-${LINEAGEVER}-${BDATE}-${BUILD_TYPE}-${dev}.zip
			${ANDROID_HOST_OUT}/bin/img_from_target_files ${ANDROID_PRODUCT_OUT}/lineage_${dev}-target_files-signed.zip ${UPLOAD_DIR}/lineage-${LINEAGEVER}-${BDATE}-${BUILD_TYPE}-${dev}-images.zip
			if [ "${IS_DYNAMIC}" == "true" ]; then
				${ANDROID_HOST_OUT}/bin/build_super_image ${ANDROID_PRODUCT_OUT}/lineage_${dev}-target_files-signed.zip ${UPLOAD_DIR}/super.img
			fi;
		elif [ "${BUILDSYSTEM}" == "twrp" ]; then
			TWRP_VER="$(grep '#define TW_MAIN_VERSION_STR' ${BASEDIR}/bootable/recovery/variables.h |awk -F\" '{ print $2 }')-0"
			cp ${ANDROID_PRODUCT_OUT}/recovery.img ${UPLOAD_DIR}/twrp-${TWRP_VER}-${BDATE}-${BUILD_TYPE}-${dev}.img;
		fi;

		# Revert special handling, if needed
		RevertPatchesFromFile patches_${BUILDSYSTEM}_${dev}.txt ${BASEDIR};
	done < ${SCRIPTSDIR}/devices.txt;

	# Revert custom patches
	RevertPatchesFromFile patches_${BUILDSYSTEM}.txt ${BASEDIR};

	# Restore environment so it won't conflict with a different android build system
	resetenv;
}

# Update or init lineage
update_lineage () {
	# Detect if the repo needs initialized.
	if [ ! -d ${LINEAGEDIR}/.repo ]; then
		logadd "Initializing Lineage repo";
		mkdir ${LINEAGEDIR};
		cd ${LINEAGEDIR};
		if [ -z ${LINEAGEMIRROR} ]; then
			repo init --git-lfs -u https://github.com/LineageOS/android.git -b ${LINEAGEBRANCH};
		else
			repo init --git-lfs -u ${LINEAGEMIRROR}/LineageOS/android.git -b ${LINEAGEBRANCH} --reference="${LINEAGEMIRROR}";
		fi;
	fi;

	# Update the repo
	logadd "Updating Lineage Repo";
	cd ${LINEAGEDIR};
	mkdir -p ${LINEAGEDIR}/.repo/local_manifests;
	cp ${MANIFESTDIR}/lineage/*.xml ${LINEAGEDIR}/.repo/local_manifests/;
	repo sync -j$(expr $(nproc) / 2 + 1) --force-sync -c --no-clone-bundle;
	if [ -d ${LINEAGEDIR}/vendor/opengapps/sources ]; then
		for i in all arm arm64; do
			git -C ${LINEAGEDIR}/vendor/opengapps/sources/$i lfs install;
			git -C ${LINEAGEDIR}/vendor/opengapps/sources/$i lfs pull;
			git -C ${LINEAGEDIR}/vendor/opengapps/sources/$i lfs prune;
		done;
	fi;
}

# Update or init twrp
update_twrp () {
	# Detect if the repo needs initialized.
	if [ ! -d ${TWRPDIR}/.repo ]; then
		logadd "Initializing TWRP repo";
		mkdir ${TWRPDIR};
		cd ${TWRPDIR};
		repo init -u https://github.com/minimal-manifest-twrp/platform_manifest_twrp_aosp.git -b ${TWRPBRANCH};
	fi;

	# Update the repo
	logadd "Updating TWRP Repo";
	cd ${TWRPDIR};
	mkdir -p ${TWRPDIR}/.repo/local_manifests;
	cp ${MANIFESTDIR}/twrp/*.xml ${TWRPDIR}/.repo/local_manifests/;
	repo sync -j$(expr $(nproc) / 2 + 1) --force-sync -c --no-clone-bundle;
}

# Main repo update function. Calls the specifc ones based on the requested build.
update_repos () {
	if [ -z $1 ] || [ "$1" == "all" ]; then
		update_lineage;
		update_twrp;
	elif [ "$1" == "lineage" -o "$1" == "twrp" ]; then
		update_$1;
	fi;
}

# Simple logging function to output to screen and save to log
logadd () {
	LOGPATH="${SCRIPTSDIR}/build.log"
	if [ "$1" == "reset" ]; then
		rm -f "${LOGPATH}";
		shift;
	fi;
	echo "$(date +"%Y/%m/%d %H:%M:%S") - ${1}" |tee -a "${LOGPATH}";
}

# Save environment variables and functions
saveenv () {
	env |awk -F'=' '{ print $1 }' > ${SCRIPTSDIR}/before.env;
	declare -F |awk '{ print $3 }' > ${SCRIPTSDIR}/before.func;
}

# Restore environment variables and functions
# This compares what was saved above with what exists now and removes the extras
# PATH is set specifically because it's the only known modified variable, all others are just added
resetenv () {
	env |awk -F'=' '{ print $1 }' > ${SCRIPTSDIR}/after.env;
	declare -F |awk '{ print $3 }' > ${SCRIPTSDIR}/after.func;
	for delenv in $(comm -13 <(sort ${SCRIPTSDIR}/before.env) <(sort ${SCRIPTSDIR}/after.env)); do
		unset "${delenv}";
	done;
	for delfunc in $(comm -13 <(sort ${SCRIPTSDIR}/before.func) <(sort ${SCRIPTSDIR}/after.func)); do
		unset -f "${delfunc}";
	done;
	PATH="${ORIGPATH}";
	rm -f ${SCRIPTSDIR}/*.{env,func}
}
