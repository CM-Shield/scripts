#!/bin/bash

source ./updater.cfg

FILENAME=$(basename ${1})
MD5SUM=$(md5sum ${1} | awk '{ print $1 }')
VERSION=$(echo ${FILENAME} |awk -F'-' '{ print $2 }')
ROMTYPE=$(echo ${FILENAME} |awk -F'-' '{ print tolower($4) }')
DEVICE=$(echo ${FILENAME} |awk -F'-' '{ print $5 }' | awk -F'.' '{ print $1 }')
BDATE=$(date -d @${2} '+%Y-%m-%d %H:%M:%S')

fwknop -A tcp/${UPLOAD_PORT} -s -D ${UPLOAD_SERVER} --key-base64-rijndael ${FWKNOP_KEY} --key-base64-hmac ${FWKNOP_HMAC}
sleep 1
scp -P${UPLOAD_PORT} ${1} ${UPLOAD_SERVER}:${UPLOAD_PATH}/${DEVICE}/
fwknop -A tcp/${UPLOAD_PORT} -s -D ${UPLOAD_SERVER} --key-base64-rijndael ${FWKNOP_KEY} --key-base64-hmac ${FWKNOP_HMAC}
sleep 1
ssh -p${UPLOAD_PORT} ${UPLOAD_SERVER} \${HOME}/bin/fix_perms.sh ${UPLOAD_PATH}/${DEVICE}/${FILENAME}

curl -H "Apikey: ${API_KEY}" -H "Content-Type: application/json" -X POST -d '{ "device": "'"${DEVICE}"'", "filename": "'"${FILENAME}"'", "md5sum": "'"${MD5SUM}"'", "romtype": "'"${ROMTYPE}"'", "url": "'"${UPDATE_SERVER}/full/${DEVICE}/${FILENAME}"'", "version": "'"${VERSION}"'", "datetime": "'"${BDATE}"'" }' "${UPDATE_SERVER}/api/v1/add_build"
