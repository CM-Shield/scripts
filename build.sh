#!/bin/bash

# Store current working directory
OLDPWD=$(pwd);

source "${BASH_SOURCE%/*}/functions.sh"
if ! ValidityCheck $0 $1; then
	exit -1;
fi;
SetVars "${BASH_SOURCE[0]}"

logadd reset "Builds started";

if [ "$2" == "alternate" ]; then
	export ALTERNATE_BUILD="true";
fi;

if [ "$2" == "offline" ]; then
	export OFFLINE="true";
else
	update_repos $1;
fi;

if [ "$2" == "cloneonly" ]; then
	logadd "Only cloning.";
elif [ -z "$1" ] || [ "$1" == "all" ]; then
	build_android lineage ${LINEAGEDIR} userdebug;
	build_android twrp ${TWRPDIR} eng;
elif [ "$1" == "lineage" ]; then
	build_android lineage ${LINEAGEDIR} userdebug;
elif [ "$1" == "twrp" ]; then
	build_android twrp ${TWRPDIR} eng;
fi;

logadd "Builds finished";

cd ${OLDPWD}
